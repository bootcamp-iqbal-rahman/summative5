describe('View component page', () => {
  beforeEach(() => {
    cy.visit('http://localhost:5173/')
  })

  it('Header', () => {
    cy.get('[data-testid="cypress-header"]').should('exist');
    cy.get('[data-testid="cypress-left-header"]').should('exist');
    cy.get('[data-testid="cypress-right-header"]').should('exist');
    cy.get('[data-testid="cypress-event-banner"]').should('exist');
    cy.get('[data-testid="cypress-animals-section"]').should('exist');
    cy.get('[data-testid="cypress-event-list"]').should('exist');
    cy.get('[data-testid="cypress-blog-list"]').should('exist');
    cy.get('[data-testid="cypress-contact-list"]').should('exist');
    cy.get('[data-testid="cypress-footer"]').should('exist');
  })
})