describe('Data Redux Action Test', () => {
    beforeEach(() => {
        cy.visit('http://localhost:5173/')
    })

    it('Animal data length', () => {
        cy.get('[data-testid="cypress-animal-redux"]').should('exist').should('have.length', 8);
    })

    it('Event data length', () => {
        cy.get('[data-testid="cypress-event-redux"]').should('exist').should('have.length', 3);
    })

    it('Blog data length', () => {
        cy.get('[data-testid="cypress-blog-redux"]').should('exist').should('have.length', 3);
    })

    it('Social data length', () => {
        cy.get('[data-testid="cypress-social-redux"]').should('exist').should('have.length', 3);
    })
});