module.exports = function (api) {
    api.cache(true);

    const presets = [
        '@babel/preset-typescript', // Menambahkan dukungan untuk TypeScript
        '@babel/preset-env' // Opsional, tergantung pada fitur JS apa yang Anda gunakan
    ];
    const plugins = [];

    if (process.env.NODE_ENV === 'test') {
        plugins.push('babel-plugin-istanbul');
    }

    env: {
        development: {
           plugins: ['istanbul'],
        },
    },

    return {
        presets,
        plugins,
    };
};
