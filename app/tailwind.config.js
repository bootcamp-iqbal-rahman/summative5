/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
    "node_modules/flowbite-react/lib/esm/**/*.js"
  ],
  theme: {
    extend: {
      backgroundImage: {
        "page": "url(/images/bg-page.gif)",
        "body": "url(/images/bg-body.gif)",
        "ticket": "url(/images/ticket.gif)",
        "active": "url(/images/navigation.gif)",
        "event": "url(/images/event.gif)",
        "animals": "url(/images/animals.gif)",
        "footer": "url(/images/bg-footer.gif)",
      },
    },
  },
  plugins: [],
}

