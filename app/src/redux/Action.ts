import { AppThunk } from './Store';
import axios from 'axios';
import {
    setAnimals,
    setBlogs,
    setEvents,
    setSocials
} from './Slice';

export const fetchAnimals = (): AppThunk => async (dispatch) => {
    try {
        const response = await axios.get('http://localhost:8080/api/animals');
        dispatch(setAnimals(response.data));
    } catch (error) {
        console.log(error);
    }
}

export const fetchBlogs = (): AppThunk => async (dispatch) => {
    try {
        const response = await axios.get('http://localhost:8080/api/blog');
        dispatch(setBlogs(response.data));
    } catch (error) {
        console.log(error);
    }
}

export const fetchEvents = (): AppThunk => async (dispatch) => {
    try {
        const response = await axios.get('http://localhost:8080/api/event');
        dispatch(setEvents(response.data));
    } catch (error) {
        console.log(error);
    }
}

export const fetchSocials = (): AppThunk => async (dispatch) => {
    try {
        const response = await axios.get('http://localhost:8080/api/social');
        dispatch(setSocials(response.data));
    } catch (error) {
        console.log(error);
    }
}