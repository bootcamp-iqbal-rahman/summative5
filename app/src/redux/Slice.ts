import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface Animal {
    id: number;
    name: string;
    url: string;
}

interface Blog {
    id: number;
    title: string;
    description: string;
    url: string;
}

interface Event {
    id: number;
    title: string;
    description: string;
    date: string;
}

interface Social {
    id: number;
    name: string;
    src: string;
    link: string;
}

interface DataState {
    animals: Animal[];
    blogs: Blog[];
    events: Event[];
    socials: Social[];
}

const initialState: DataState = {
    animals: [],
    blogs: [],
    events: [],
    socials: [],
};

const dataSlice = createSlice({
    name: 'data',
    initialState,
    reducers: {
        setAnimals: (state, action: PayloadAction<Animal[]>) => {
            state.animals = action.payload;
        },
        setBlogs: (state, action: PayloadAction<Blog[]>) => {
            state.blogs = action.payload;
        },
        setEvents: (state, action: PayloadAction<Event[]>) => {
            state.events = action.payload;
        },
        setSocials: (state, action: PayloadAction<Social[]>) => {
            state.socials = action.payload;
        },
    },
});

export const { setAnimals, setBlogs, setEvents, setSocials } = dataSlice.actions;
export default dataSlice.reducer;
