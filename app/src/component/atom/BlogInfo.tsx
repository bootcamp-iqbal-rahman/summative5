export const BlogInfo: React.FC<BlogInfoComponent> = (props) => {
    return (
        <div className="lg:flex mt-4 space-x-4" data-testid="cypress-blog-redux">
            <img src={props.src}></img>
                <div>
                    <h1 className="text-[#875316] font-semibold">{props.title}</h1>
                    <p className="text-[#875316]">{props.description}</p>
                </div>
        </div>
    )
}