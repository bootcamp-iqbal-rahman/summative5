export const ContactInfo: React.FC<ContactInfoComponent> = (props) => {
    return (
        <div className="flex" data-testid="cypress-social-redux">
            <img src={props.src}></img>
                <a href={props.url}>{props.title}</a>
        </div>
    )
}