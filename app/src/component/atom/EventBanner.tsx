export const EventBanner: React.FC = () => {
    return (
        <div className="m-6 bg-event" data-testid="cypress-event-banner">
            <div className="m-2 py-3 flex items-end">
                <h1 className="text-[#c39f63] font-bold text-2xl lg:text-3xl">&ensp;Special Events: &ensp;</h1>
                <span className="text-[#AFAFAF] font-normal text-base text-sm lg:text-xl">This website has been designed by
                    <span className="text-white font-bold"> Nexsoft Bootcamp</span>
                </span>
            </div>
        </div>
    )
}