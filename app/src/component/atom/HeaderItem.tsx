export const HeaderItem: React.FC<HeaderItemComponent> = (props) => {
    return (
        <div>
            <h1 className="font-bold">{props.title}</h1>
            <p>{props.description}</p>
        </div>
    )
}