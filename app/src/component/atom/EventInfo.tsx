export const EventInfo: React.FC<EventInfoComponent> = (props) => {
    return (
        <div className="mt-2" data-testid="cypress-event-redux">
            <h2 className="text-[#8F0417]">{props.title}</h2>
            <p className="text-[#875316]">{props.description}</p>
        </div>
    )
}