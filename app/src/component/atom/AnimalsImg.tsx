export const AnimalsImg: React.FC<AnimalsImgComponent> = (props) => {
    return (
        <div className="flex flex-col text-center" data-testid="cypress-animal-redux">
            <img src={props.src}></img>
                <p className="text-sm">{props.title}</p>
        </div>
    )
}