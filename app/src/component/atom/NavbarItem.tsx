export const NavbarItem: React.FC<NavbarItemComponent> = (props) => {
    return (
        <li className={props.status ? "bg-active py-[14px] px-[17px] mr-[-10px] text-black" : ""}><a>{props.title}</a></li>
    )
} 