import React, { Suspense } from "react";
import { Footer } from "../organism/Footer";
import { Header } from "../organism/Header"
const Body = React.lazy(() => import('../organism/Body'));

export const HomePage: React.FC = () => {
    return (
        <div>
            <div className="flex justify-center">
                <div className="min-h-screen bg-page max-w-[960px]">
                    <Header />
                    <Suspense fallback={<div>Loading...</div>}>
                        <Body />
                    </Suspense>
                </div>
            </div>

            <Footer />
        </div>
    );
}