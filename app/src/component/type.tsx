interface HeaderItemComponent {
    title: string;
    description: string;
}

interface NavbarItemComponent {
    title: string;
    status?: boolean;
}

interface AnimalsImgComponent {
    title: string;
    src: string;
}

interface EventInfoComponent {
    title: string;
    description: string;
}

interface BlogInfoComponent {
    title: string;
    description: string;
    src: string;
}

interface ContactInfoComponent {
    title: string;
    src: string;
    url: string;
}

interface Window {
    Cypress: any;
    store: any;
}