import { NavbarItem } from "../atom/NavbarItem"

export const RightHeader: React.FC = () => {
    return (
        <div data-testid="cypress-right-header">
            <div id="navbar">
                <ul className="text-white font-medium hidden lg:flex justify-between items-center min-h-20 mx-4">
                    <NavbarItem 
                        title={"Home"} status={true}/>
                    <NavbarItem 
                        title={"The Zoo"} />
                    <NavbarItem 
                        title={"Visitors Info"} />
                    <NavbarItem 
                        title={"Tickets"} />
                    <NavbarItem 
                        title={"Events"} />
                    <NavbarItem 
                        title={"Gallery"} />
                    <NavbarItem 
                        title={"Contact Us"} />
                </ul>
            </div>
            <div className="mt-28 lg:mt-0 max-w-[325px] lg:max-w-none ">
                <img src="/images/lion-family.jpg"></img>
            </div>
        </div>
    )
}