import { useEffect } from "react"
import { useAppDispatch, useAppSelector } from "../../redux/Hooks"
import { RootState } from "../../redux/Store"
import { ContactInfo } from "../atom/ContactInfo"
import { fetchSocials } from "../../redux/Action"

export const ContactList: React.FC = () => {
    const dispatch = useAppDispatch()
    const socials = useAppSelector((state: RootState) => state.data.socials) || []
    
    useEffect(() => {
        dispatch(fetchSocials())
    }, [])

    return (
        <div data-testid="cypress-contact-list">
            <h1 className="text-[#8F0417] font-bold text-xl">Connect</h1>
            <div>
                {socials.map((item) => (
                    <ContactInfo 
                        key={"social-" + item.id}
                        title={item.name}
                        src={item.src} 
                        url={item.link} />
                ))}
            </div>
            <div className="mt-4">
                <p className="text-[#8F0417]">Subscribe to our</p>
                <h1 className="text-[#8F0417] font-bold text-xl">NEWSLETTER</h1>
                <input type="email" placeholder="your email here..."></input>
            </div>
            <img className="ml-8 mt-12 lg:mt-0 lg:ml-6" src="/images/penguin2.jpg"></img>
        </div>
    )
}