import { useEffect } from "react"
import { useAppDispatch, useAppSelector } from "../../redux/Hooks"
import { EventInfo } from "../atom/EventInfo"
import { RootState } from "../../redux/Store"
import { fetchEvents } from "../../redux/Action"

export const EventList: React.FC = () => {
    const dispatch = useAppDispatch()
    const events = useAppSelector((state: RootState) => state.data.events) || []
    
    useEffect(() => {
        dispatch(fetchEvents())
    }, [])

    return (
        <div className="max-w-64 overflow-auto max-h-[380px]" data-testid="cypress-event-list">
            <h1 className="text-[#8F0417] font-bold text-xl">Events</h1>
            {events.map((item) => (
                <EventInfo
                    key={"event-" + item.id}
                    title={item.date}
                    description={item.description} />
            ))}
        </div>
    )
}