import { HeaderItem } from "../atom/HeaderItem"

export const LeftHeader: React.FC = () => {
    return (
        <div className="max-w-[340px]" data-testid="cypress-left-header">
            <img src="/images/logo-page.jpg"></img>
            <div className="flex text-[#553B22] ml-8 leading-5 mt-6 space-x-2">
                <HeaderItem 
                    title={"Live"} 
                    description={"have fun in your visit"} />
                <HeaderItem 
                    title={"Love"} 
                    description={"Donate for the animals"} />
                <HeaderItem 
                    title={"Learn"} 
                    description={"get to know the animals"} />
            </div>
            <div className="ml-6 flex items-center text-white font-black mt-8">
                <h1 className="py-3 px-8 bg-ticket">Buy Tickets / Check Events</h1>
            </div>
        </div>
    );
}