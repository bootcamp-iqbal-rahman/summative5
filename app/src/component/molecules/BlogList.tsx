import { useEffect } from "react"
import { useAppDispatch, useAppSelector } from "../../redux/Hooks"
import { RootState } from "../../redux/Store"
import { BlogInfo } from "../atom/BlogInfo"
import { fetchBlogs } from "../../redux/Action"

export const BlogList: React.FC = () => {
    const dispatch = useAppDispatch()
    const blogs = useAppSelector((state: RootState) => state.data.blogs) || []
    
    useEffect(() => {
        dispatch(fetchBlogs())
    }, [])

    return (
        <div data-testid="cypress-blog-list">
            <h1 className="text-[#8F0417] font-bold text-xl">Blog : Curabitur sodales</h1>
            <p className="text-[#875316] font-semibold">This website template has been designed by nexsoft.co.id for you..</p>
            <div className="overflow-y-scroll max-h-[200px] lg:max-h-[300px]">
                {blogs.map((item) => (
                    <BlogInfo 
                        key={"blog-" + item.id}
                        title={item.title} 
                        description={item.description}
                        src={item.url} />
                ))}
            </div>
        </div>
    )
}