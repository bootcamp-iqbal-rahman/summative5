import { useEffect } from "react"
import { useAppDispatch, useAppSelector } from "../../redux/Hooks"
import { RootState } from "../../redux/Store"
import { AnimalsImg } from "../atom/AnimalsImg"
import { fetchAnimals } from "../../redux/Action"

export const AnimalsSec: React.FC = () => {
    const dispatch = useAppDispatch()
    const animals = useAppSelector((state: RootState) => state.data.animals) || []
    
    useEffect(() => {
        dispatch(fetchAnimals())
    }, [])

    return (
        <div className="bg-animals" data-testid="cypress-animals-section">
            <h1 className="ml-8 font-bold text-3xl text-[#8F0417] pt-6">Meet our Animals</h1>
            <div className="flex justify-between mx-8 pt-2 pb-2">
                {animals.map((item) => (
                    <AnimalsImg 
                        key={"animal-" + item.id}
                        title={item.name} 
                        src={item.url} />
                ))}
            </div>
        </div>
    )
}