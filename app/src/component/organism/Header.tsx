import React from "react";
import { LeftHeader } from "../molecules/LeftHeader";
import { RightHeader } from "../molecules/RightHeader";

export const Header: React.FC = () => {
    return (
        <div className="flex" data-testid="cypress-header">
            <LeftHeader />
            <RightHeader />
        </div>
    );
}