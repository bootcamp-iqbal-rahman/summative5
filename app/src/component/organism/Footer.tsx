export const Footer: React.FC = () => {
    return (
        <div className="flex justify-center bg-footer" data-testid="cypress-footer">
            <div className="flex space-x-28 max-w-[960px]">
                <div className="flex">
                    <img src="/images/animal-kingdom.jpg"></img>
                        <div className="text-sm text-[#AFAFAF] pt-8 leading-7">
                            <p>Ellentesque habitant morbi tristique</p>
                            <p>senectus et 0000</p>
                            <p>285 067-39 282 / 5282 9273 999</p>
                            <p>email@nexsoft.co.id</p>
                        </div>
                </div>
                <div className="flex text-sm text-[#AFAFAF] pt-8 space-x-4 leading-7">
                    <div>
                        <ul>
                            <li>Home</li>
                            <li>The Zoo</li>
                            <li>Blog</li>
                        </ul>
                    </div>
                    <div>
                        <ul>
                            <li>Tickets</li>
                            <li>Events</li>
                            <li>Gallery</li>
                        </ul>
                    </div>
                </div>
                <div className="text-sm text-[#AFAFAF] pt-8 leading-7">
                    <p>Live: Have fun in your visit</p>
                    <p>Love: Donate for the animals</p>
                    <p>Learn: Get to know the animals</p>
                </div>
            </div>
        </div>
    )
}