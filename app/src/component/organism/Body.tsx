import { EventBanner } from "../atom/EventBanner"
import { AnimalsSec } from "../molecules/AnimalsSec"
import { BlogList } from "../molecules/BlogList"
import { ContactList } from "../molecules/ContactList"
import { EventList } from "../molecules/EventList"

const Body: React.FC = () => {
    return (
        <>
            <EventBanner />
            <AnimalsSec />
            <div className="m-8 flex justify-between space-x-4">
                <EventList />
                <BlogList />
                <ContactList />
            </div>
        </>
    )
}

export default Body;