package id.co.nexsoft.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.restapi.model.Animals;

public interface AnimalRepository extends  JpaRepository<Animals, Integer> {
    
}
