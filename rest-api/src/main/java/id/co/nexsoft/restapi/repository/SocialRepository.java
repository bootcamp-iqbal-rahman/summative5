package id.co.nexsoft.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.restapi.model.Social;

public interface SocialRepository extends  JpaRepository<Social, Integer> {
    
}
