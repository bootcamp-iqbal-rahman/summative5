package id.co.nexsoft.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.restapi.model.Blog;

public interface BlogRepository extends  JpaRepository<Blog, Integer> {
    
}
