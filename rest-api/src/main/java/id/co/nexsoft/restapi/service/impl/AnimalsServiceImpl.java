package id.co.nexsoft.restapi.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.Animals;
import id.co.nexsoft.restapi.repository.AnimalRepository;
import id.co.nexsoft.restapi.service.DefaultService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class AnimalsServiceImpl implements DefaultService<Animals> {
    @Autowired
    private AnimalRepository repo;
    
    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Animals> getAllData() {
        return repo.findAll();
    }

    @Override
    public Optional<Animals> getDataById(int id) {
        return repo.findById(id);
    }

    @Override
    public boolean deleteDataById(int id) {
        Optional<Animals> Animals = repo.findById(id);
        if(!Animals.isPresent()) {
            return false;
        }
        repo.deleteById(id);
        return true;
    }

    @Override
    public Animals putData(Animals data, int id) {
        Optional<Animals> Animals = repo.findById(id);
        if(!Animals.isPresent()) {
            return null;
        }
        data.setId(id);
        return repo.save(data);
    }

    @Override
    public Animals postData(Animals data) {
        return repo.save(data);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, int id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE Animals SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }
}