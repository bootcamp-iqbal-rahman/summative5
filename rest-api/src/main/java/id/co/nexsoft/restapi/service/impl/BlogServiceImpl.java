package id.co.nexsoft.restapi.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.Blog;
import id.co.nexsoft.restapi.repository.BlogRepository;
import id.co.nexsoft.restapi.service.DefaultService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class BlogServiceImpl implements DefaultService<Blog> {
    @Autowired
    private BlogRepository repo;
    
    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Blog> getAllData() {
        return repo.findAll();
    }

    @Override
    public Optional<Blog> getDataById(int id) {
        return repo.findById(id);
    }

    @Override
    public boolean deleteDataById(int id) {
        Optional<Blog> Blog = repo.findById(id);
        if(!Blog.isPresent()) {
            return false;
        }
        repo.deleteById(id);
        return true;
    }

    @Override
    public Blog putData(Blog data, int id) {
        Optional<Blog> Blog = repo.findById(id);
        if(!Blog.isPresent()) {
            return null;
        }
        data.setId(id);
        return repo.save(data);
    }

    @Override
    public Blog postData(Blog data) {
        return repo.save(data);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, int id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE Blog SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }
}