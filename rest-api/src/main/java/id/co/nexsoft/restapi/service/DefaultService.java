package id.co.nexsoft.restapi.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface DefaultService<T> {
    List<T> getAllData();
    Optional<T> getDataById(int id);
    boolean deleteDataById(int id);
    T putData(T data, int id);
    T postData(T data);
    void patchData(Map<String, Object> data, int id);
}
