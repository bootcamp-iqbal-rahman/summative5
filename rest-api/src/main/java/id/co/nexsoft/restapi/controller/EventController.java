package id.co.nexsoft.restapi.controller;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.hibernate.JDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.restapi.model.Event;
import id.co.nexsoft.restapi.service.DefaultService;
import id.co.nexsoft.restapi.utils.ErrorMessage;
import id.co.nexsoft.restapi.utils.StatusCode;
import jakarta.validation.Valid;

@RestController
@RequestMapping("api/event")
@Validated
public class EventController extends StatusCode {
    @Autowired
    private DefaultService<Event> service;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM d");

    @GetMapping
    public ResponseEntity<?> getAllData() {
        List<Map<String, Object>> result = new ArrayList<>();
        List<Event> data = service.getAllData();

        for (Event d : data) {
            result.add(changeDate(d));
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public Map<String, Object> changeDate(Event data) {
        Map<String, Object> newData = new HashMap<>();
        newData.put("id", data.getId());
        newData.put("title", data.getTitle());
        newData.put("description", data.getDescription());
        newData.put("date", data.getDate().format(formatter));
        return newData;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getDataById(@PathVariable int id) {
        Optional<Event> data = service.getDataById(id);
        if (data.isPresent()) {
            return new ResponseEntity<>(changeDate(data.get()), HttpStatus.OK);
        }
        return new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND);
    }

    @PostMapping
    public ResponseEntity<?> postData(@Valid @RequestBody Event data) {
        service.postData(data);
        return new ResponseEntity<>(get201(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putData(@Valid @RequestBody Event data, @PathVariable int id) {
        Event dataPut = service.putData(data, id);
        return (dataPut == null) ?
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(dataPut, HttpStatus.OK);
    }   

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteData(@PathVariable int id) {
        boolean dataDelete = service.deleteDataById(id);
        return (!dataDelete) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(get200(), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> patchData(@PathVariable int id, @RequestBody Map<String, Object> data) {
        Optional<Event> dataPatch = service.getDataById(id);
        try {
            if (!dataPatch.isPresent()) {
                return new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND);
            }
            service.patchData(data, id);
            return new ResponseEntity<>(get200(), HttpStatus.OK);
        } catch (JDBCException e) {
            return new ResponseEntity<>(
                getCustom(CODE[4], ErrorMessage.extractJDBCErrorMessage(e)), 
                HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}
