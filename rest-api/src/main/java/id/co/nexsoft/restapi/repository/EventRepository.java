package id.co.nexsoft.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.restapi.model.Event;

public interface EventRepository extends  JpaRepository<Event, Integer> {
    
}
