package id.co.nexsoft.restapi.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import id.co.nexsoft.restapi.model.Animals;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest
@AutoConfigureMockMvc
public class AnimalsControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void getAllData() throws Exception {
        this.mockMvc.perform(get("/api/animals"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void getDataById() throws Exception {
        this.mockMvc.perform(get("/api/animals/{id}", 1))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void getDataByNullId() throws Exception {
        this.mockMvc.perform(get("/api/animals/{id}", 100))
            .andDo(print())
            .andExpect(status().isNotFound());
    }

    @Test
    void postData() throws Exception {
        Animals mockAnimals = new Animals(
            "title test", "url test"
        );

        mockMvc.perform(post("/api/animals")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockAnimals))
        ).andExpect(status().isCreated());
    }

    @Test
    void postErrorData() throws Exception {
        Map<String, Object> mockAnimals = new HashMap<>();
        mockAnimals.put("names", 0);

        mockMvc.perform(post("/api/animals")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockAnimals))
        ).andExpect(status().isBadRequest());
    }

    @Test
    void putData() throws Exception {
        Animals mockAnimals = new Animals(
            "title test", "url test"
        );
        
        mockMvc.perform(put("/api/animals/{id}", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockAnimals))
        ).andExpect(status().isOk());
    }

    @Test
    void patchData() throws Exception {
        Map<String, Object> mockAnimals = new HashMap<>();
        mockAnimals.put("name", "test patch");
        
        mockMvc.perform(patch("/api/animals/{id}", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockAnimals))
        ).andExpect(status().isOk());
    }

    @Test
    void patchNullData() throws Exception {
        Map<String, Object> mockAnimals = new HashMap<>();
        mockAnimals.put("names", "test patch");
        
        mockMvc.perform(patch("/api/animals/{id}", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockAnimals))
        ).andExpect(status().isUnprocessableEntity());
    }

    @Test
    void patchNullId() throws Exception {
        Map<String, Object> mockAnimals = new HashMap<>();
        mockAnimals.put("name", "test patch");
        
        mockMvc.perform(patch("/api/animals/{id}", 100)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockAnimals))
        ).andExpect(status().isNotFound());
    }

    @Test
    void putDataNullId() throws Exception {
        Animals mockAnimals = new Animals(
            "title test", "description test"
        );
        
        mockMvc.perform(put("/api/animals/1000")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mockAnimals))
        ).andExpect(status().isNotFound());
    }

    @Test
    void deleteData() throws Exception {
        mockMvc.perform(delete("/api/animals/{id}", 3)
                ).andExpect(status().isOk());
    }

    @Test
    void deleteNullData() throws Exception {
        mockMvc.perform(delete("/api/animals/{id}", 1000)
                ).andExpect(status().isNotFound());
    }

    private String asJsonString(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper.writeValueAsString(object);
    }
}
